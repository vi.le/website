---
author: Vincent Lequertier
title: "SymPy: a powerful math library"
date: 2024-03-06T13:00:00+02:00
katex: true
Tags:
    - Free Software
    - Tutorial
categories:
- General
---

SymPy is a lightweight symbolic mathematics library for Python under the
3-clause BSD license that can be used either as a library or in an interactive
environment. It features symbolic expressions, solving equations, plotting and
much more!

<!--more-->

## Creating and using functions

Creating a function is as simple as using variables and putting
them into a mathematical expression:

{{< highlight python "linenos=table">}}
from sympy.abc import x, y

f = 2 * x**2 + 3 * y + 10
f.subs(((x, 4), (y, 5))) # yields 57
{{< / highlight >}}

Line 1 imports the `x` and `y`
[Symbols](https://docs.sympy.org/latest/modules/core.html#module-sympy.core.symbol)
from the `abc` collection of Symbols, line 3 creates the `f` function which is
$2x^2 + 3y + 10$, and line 4 evaluates `f` with values 3 and 5 for `x` and `y`,
respectively.

SymPy exports a large
[list](https://docs.sympy.org/latest/modules/functions/elementary.html) of
mathematical functions for more complex expressions such logarithm, exponential,
etc., and is able to analyze the fucntion's characteristics over arbitrary
intervals:

{{< highlight python "linenos=table">}}
from sympy import Interval, is_monotonic, minimum, sin

f = sin(x)
is_monotonic(f, Interval(0, 10)) # yields False
minimum(f, x, Interval(0, 10) ) # yields -1
{{< / highlight >}}

## Plotting

SymPy is able to easily plot functions on a given interval:

{{< highlight python "linenos=table">}}
from sympy.plotting import plot

f = 1 / (1 + sympy.exp(-x))
p1 = plot(f, sympy.diff(f), (x, -10, 10), show=False, legend=True)
p1[0].label = 'sigmoid'
p1[1].label = 'derivative of sigmoid'
p1.show()
{{< / highlight >}}

Line 1 imports the
[`plot()`](https://docs.sympy.org/latest/modules/plotting.html#sympy.plotting.plot.plot)
function, line 3 creates the `f` Function object from the sigmoid 
expression $\frac{1}{1+\exp^{-x}}$, line 4 plots `f` and its derivative between
-5 and 5, lines 5-6 set the legends labels and lines 7 shows the created plot:

{{< figure src="/imgs/sigmoid.png" alt="Sigmoid and its derivative." caption="Sigmoid and its derivative.">}}

In a few lines of code, the library allows one to create a mathematical
function, do operations on it like computing its derivative and plot the result.
Moreover, as SymPy uses [Matplotlib](https://matplotlib.org/) under the hood by
default, the plots are modifiable using the Matplotlib API.

SymPy also supports 3D plots with the
[`plot3d()`](https://docs.sympy.org/latest/modules/plotting.html#sympy.plotting.plot.plot3d)
function which helps understanding the relationship between two variables and an
outcome. The following code computes the energy of the recoiling electron after
Compton scattering depending on the initial photon energy and the scattering
angle (in degrees) using the [Compton scattering
formula](https://en.wikipedia.org/wiki/Compton_scattering) : $$\beta = \arccos
\left(1 -  \frac{Ee * 511}{E0 * (E0 - Ee)}\right) * \frac{180}{3.14159}$$

{{< highlight python "linenos=table">}}
from sympy.plotting import plot3d

E0, Ee = sympy.symbols('E0 Ee')
beta = sympy.acos(1.0 - Ee * 511 / (E0 * (E0 - Ee))) * 180 / 3.14159
p1 = plot3d(beta, (E0, 1, 511), (Ee, 0, 400), show=False, size=(10,10))
p1.zlabel=r'$\beta$ angle'
p1.show()
{{< / highlight >}}

Line 1 imports the `plot3d` function, line 3 creates the `E0` (the initial
photon energy) and `theta` custom variables[^1], line 4 defines the `beta`
function which depends on the `E0` and `Ee` variables, line 6 does the
plotting and defines the axes intervals, line 7 sets the axes' labels and line 8
displays the following figure:

{{< figure src="/imgs/compton.png" alt="Compton angle depending on E0 and Ee." caption="Compton angle depending on E0 and Ee.">}}


## Solving equations

SymPy also has an equation solver for algebraic equations and equations systems:
SymPy considers the equations strings to be equal to zero for ease of use.

{{< highlight python "linenos=table">}}
f = 3 * x - 1
sympy.solveset(f, x) # yields 1/3

eq1 = x + 5 + y - 2
eq2 = -3 * x + 6 * y - 3
sympy.solve((eq1, eq2), (x, y)) # yields {x: -7/3, y: -2/3}
{{< / highlight >}}

The equation solvers also works for boolean logic, where SymPy tells what should
be the truth values of the variables to satisfy a boolean expression:

{{< highlight python "linenos=table">}}
sympy.satisfiable(x & ~y & (x | y)) # yields {x: True, y: False}
{{< / highlight >}}

An unsatisfiable expression yields `False`.

## Geometry

SymPy also features a [geometry](docs.sympy.org/latest/modules/geometry/) module allowing to perform geometric operations such as [Points](https://docs.sympy.org/latest/modules/geometry/points.html#sympy.geometry.point.Point), [Segments](https://docs.sympy.org/latest/modules/geometry/lines.html#sympy.geometry.line.Segment) and [Polygons](https://docs.sympy.org/latest/modules/geometry/polygons.html#sympy.geometry.polygon.Polygon). It's 

{{< highlight python "linenos=table">}}
from sympy.geometry import Point, Segment, Polygon
from spb import plot_geometry

p = Polygon(Point(0, 0), Point(0, 3), Point(3, 3), Point(3, 0))
s = Segment(Point(1, 3), Point(4, 0))
p1 = plot_geometry(p,s, is_filled=False, legend=False)
p.intersect(s) # yields {Point2D(1, 3), Point2D(3, 1)}
{{< / highlight >}}

Line 1 imports `classes` from the `geometry` module, Line 2 import the SymPy plotting backends library, which contains additional plotting features needed to plot geometric objects, line 3 and 4 create a polygon and a segment, respectively, line 6 plots the figure and line 7 returns the intersection points of the polygon and the segment. Line 6 produces the following figure:

{{< figure src="/imgs/sympy_geometry.png" alt="Compton angle depending on E0 and Ee." caption="Compton angle depending on E0 and Ee.">}}


The SymPy library is thus easy to use and suitable for various applications.
Because it's Free Software, anyone can use, share, study and improve it for
whatever purpose, just like mathematics.


[^1]: We could use the `x` and `y` variables here but having custom variables
    names makes reading the code easier.
