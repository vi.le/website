---
author: Vincent Lequertier
date: 2018-03-30 11:20:23+00:00
draft: false
title: "As developpers we are doing the easy work"
categories:
- General
---


We should not think that programming is complicated. It is often the easiest part of an IT project, because one simply needs to communicate with a computer. The communication between human beings is far more complicated. Computers are the most predictable things in the universe. Humans are [unpredictable by nature](https://www.psychologytoday.com/us/blog/ulterior-motives/200811/unpredictability-is-in-our-nature). They can lie, change mood or theirs opinions multiple times, decline an offer because they had a bad day or due to the weather, or the horoscope, you name it. Computers happily follow orders given to them. They are as smart as their users.

As a growing industry, there are plenty of resources and documentation available online. They are, at the time of writing, more than 15 million questions already asked on StackOverflow, a Question & Answer website for programmers.

![Number of questions in SO is 15562837](/imgs/so-questions.png#center)


It is easy to fix issues. Usually, when someone has a problem, there is a good chance that his or her problem has already been solved and the solution documented somewhere. Even if a problem is really hard, it is often easy to test multiple solutions and to undo things. When you communicate with humans, you have only one chance. You cannot undo a conversation. I'm not saying that software development is easy in general ; this is the most complex step. We need to understand or guess user needs, define the feasibility and profitability of projects. Then all of that must be converted into a technical process for the development itself, and to a marketing campaign. This implies a lot of exchanges and therefore [as many possibilities of misunderstanding](https://xavierhahn.files.wordpress.com/2012/05/famous-design-cartoon11.jpg). What I am talking about is the code writing. This requires skills to learn, habits to take, procedures to understand and tools to master. But this is definitely doable.

We have to stop believing that programming is hard and requires a Computer Science degree. This is an activity accessible to all.
