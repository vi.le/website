---
author: Vincent Lequertier
date: 2018-04-21 16:47:23+00:00
draft: false
title: "On trading security for convenience"
categories:
- General
tags:
- Security
---


If we have to choose between a convenient system and a secure one, we often pick the former rather than the latter. The reason is mainly psychological. Several scientific studies have shown that we prefer instant gratification over delayed gratification, because that’s how our brains are wired. We are surrounded by instant gratification, our day-to-day actions like our hobbies, usage of social media, got us hooked on having a quick feedback. We are naturally and culturally inclined to shortsighted behaviors. This is why we like to build convenient things without caring about their security. Convenience provides instant gratification, and a short time to market. Security do not. The instant gratification of launching an app even if it is known to be insecure is so tempting compared to the delayed gratification of securing it.

It is hard to measure the return on investment of making a piece of software secure. Security is not about numbers, it is a long-term process and often the only number associated with it is its cost. Only a few persons are even able to understand whether a software is secure. On the other hand, convenience is easy to measure, because it can be connected to the number of users (nobody likes a software if it is not convenient), to the time spent managing a software, or to the number of people needed to maintain it. This is why security is often on the “nice-to-have” list, but hopefully things are changing as security is slowly becoming a marketing argument. Security getting mainstream will be reinforced by the entry into force of GDPR, the new European regulation that will force companies to secure their applications which process personal data.

I look forward to see security on the "must-have" list.
