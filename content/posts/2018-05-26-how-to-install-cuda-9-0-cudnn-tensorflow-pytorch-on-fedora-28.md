---
author: Vincent Lequertier
date: 2018-05-26 12:38:13+00:00
draft: false
title: "How to install cuda 9.0, cudnn, tensorflow, pytorch on Fedora 28"
categories:
- Tutorial
tags:
- Deep Learning
- Tutorial
---


This procedure has been tested on Fedora 28, on a HP laptop with this graphical card: NVIDIA Corporation GP107M GeForce GTX 1050 Mobile (rev a1).

The commands have to be run as the root user. This tutorial assumes the nvidia driver is already working.



### Install pip



{{< highlight none >}}
dnf install python3-pip
{{< / highlight >}}


### Install Cuda



We install cuda 9.0 as it is the latest version supported by tensorflow at the time of writing.

{{< highlight bash >}}
wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run
sh cuda_9.0.176_384.81_linux-run
{{< / highlight >}}

You will have a few choices. Select "install cuda", don't install samples or drivers.Select "create a symbolic link". You can set the TMPDIR en variable before executing the script to select the location of the temporary folder used during the installation. Can be used if the default, /tmp, is not large enoguh.



### Install cudnn



{{< highlight none >}}
dnf install python3-pip
wget http://developer.download.nvidia.com/compute/redist/cudnn/v7.0.5/cudnn-9.0-linux-x64-v7.tgz
tar -xvf cudnn-9.0-linux-x64-v7.tgz
cp cudnn.h /usr/local/cuda-9.0/include/cudnn.h
cp libcudnn.so.7.0.5 /usr/local/cuda/lib64/libcudnn.so.7.0.5
ln -s /usr/local/cuda/lib64/libcudnn.so.7.0.5 /usr/local/cuda/lib64/libcudnn.so.7
ln -s /usr/local/cuda/lib64/libcudnn.so.7 /usr/local/cuda/lib64/libcudnn.so
{{< / highlight >}}


### Update environment variables



So your compiler can find the installed libraries:

{{< highlight none >}}
cat >> ~/.bashrc << EOF
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64
export CUDA_HOME=/usr/local/cuda
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/cuda/samples/common/inc
EOF
{{< / highlight >}}


### Install GCC 5



Cuda 9.0 requires a GCC version inferior <= 6. We install GCC 5 from mageia packages as Fedora does not provide old packages for GCC.

{{< highlight none >}}
cat >> ~/.bashrc << EOF
wget https://rpmfind.net/linux/mageia/distrib/6/x86_64/media/core/updates/gcc-c++-5.5.0-1.mga6.x86_64.rpm \
     https://rpmfind.net/linux/mageia/distrib/6/x86_64/media/core/updates/gcc-5.5.0-1.mga6.x86_64.rpm \
     https://rpmfind.net/linux/mageia/distrib/6/x86_64/media/core/updates/libstdc++-static-devel-5.5.0-1.mga6.x86_64.rpm \
     https://rpmfind.net/linux/mageia/distrib/6/x86_64/media/core/updates/libstdc++-devel-5.5.0-1.mga6.x86_64.rpm

rpm -ivh --force --nodeps *.rpm
{{< / highlight >}}
`
You can test the installation by running:

{{< highlight none >}}
cd /usr/local/cuda/samples/1_Utilities/deviceQuery
make
./deviceQuery
{{< / highlight >}}

You should see  `Result = PASS` in the output



### Install pytorch



{{< highlight none >}}
pip3 install http://download.pytorch.org/whl/cu90/torch-0.4.0-cp36-cp36m-linux_x86_64.whl
pip3 install torchvision
{{< / highlight >}}



### Install Tensorflow

{{< highlight none >}}
pip3 install tensorflow-gpu
{{< / highlight >}}

