---
author: Vincent Lequertier
date: 2019-05-02 09:05:54+00:00
draft: false
title: "Why is online privacy understated"
categories:
- General
tags:
- Privacy
---


There's a lot of guides explaining how to protect your online privacy, but none of them tell why they exist in the first place. They exist because privacy is understated. We don't value it enough. Here are the reasons.



### Threats to privacy are not obvious



Despite recent attempts to regulate online data processing (e.g the [GDPR](https://gdpr-info.eu/) in the EU) as well as privacy breaches, it's still not clear why all of that threatens privacy. By collecting data on individuals, corporations can have an accurate description of who you are, For example, in [this study](https://www.pnas.org/content/pnas/112/4/1036.full.pdf), researchers show that the profiling of Facebook (based on likes) is a more accurate representation of people's personalities than their friends view. On top of that, they also explain that this digital profiling is very powerful. It does a great job at predicting offline data: political opinions, health status, and much more. This is one reason why online privacy should not be overlooked. By tracking you and gathering always more data, private companies can draw a model that represents you and gives them a lot of power.

Also, the last thing data collectors want you to know is that this profiling is literally everywhere online. Among the top 1 million web sites (according to [this paper](https://chromium.woolyss.com/f/OpenWPM-1-million-site-tracking-measurement.pdf)), 80% use google tracking technologies and roughly 40% use the Facebook ones. All this data harvesting records everything we do, everywhere we go online.



### What we get from privacy is taken for granted



Privacy is what balances power between the state and its citizens. This has been thoroughly described by H.G Orwell in his famous book "[1984](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four)". People are controlled by the state by invading their privacy, in a number of ways. In every room there's a camera that records what is done and said and if one action (or even a though!) is not aligned with the interests of the state, its author is taken down by the state police. Monitoring everything allows the state to make sure there's no revolt. On the internet, the same thing can happen. For example, Google [is helping law enforcement](https://www.nytimes.com/interactive/2019/04/13/us/google-location-tracking-police.html) by handing over people's location data. From privacy, you get parity with the state.



### It's impossible to achieve privacy online, right?



Although people can be convinced that privacy matters (of course it does!), they can imagine that privacy is a chimera, impossible to achieve unless they're willing to live in a cave without any connected devices. And the endless stream of privacy scandals doesn't help breaking down this misconception. Your online data do not have to be shared with everyone without your consent. As data regulation keep improving, data harvesters become legally incentivized to be clear about what they're doing and to make sure the users approve that.

Also, more and more privacy-focused platforms are developed as alternatives to invasive ones, For example, [Mastodon](https://joinmastodon.org/) (an alternative to Facebook) user base keeps increasing, [DuckDuckGo](https://duckduckgo.com/) (alternative to Google) reaches [35M daily searches](https://duckduckgo.com/traffic). Those platforms do not include trackers or if they do, they're transparent about it.



### Privacy is not only a personal matter



Part of the goal of data collection is to make connections between people. They're plenty of solutions to achieve this. The easiest one is to gather friends and contact data. As most people have a Facebook account (and tend to be online friends with those with whom they are offline friends too), it's easy to infer what one might like or think based on relations to others. The same can be said for phone contacts or any messaging system. Also, by observing location data, one can figure out relationships. If a group of people often meets at the same places, they must be friends, Data aggregators can after guess someone's interest with the group's profile. Location data can easily be retrieved from smartphones.

Tracking spreads like a virus. Once someone gets tracked, the system can create a profile of the person, but can also know better the person's contacts. This is why privacy should be everybody's concern.



### We don't want to accept the trade-offs that come with protecting privacy



One might use privacy-invasive tech because besides tracking, it is pretty good, with convenient and easy to use features. Before accepting to be tracked in exchange for a "good" service, there's a couple of issues to consider. First, we might not know how far the tracking goes. One may be comfortable with sharing his or her device information with third-parties, but not all emails data and metadata. We might accept to share the city where we live, but not real-time location. Often, what is monitored is intentionally blurry so we don't know what we're giving away. 

Also, tracking can make the service itself bad. Every bit of engineering that goes into tracking users for the purpose if showing "relevant" ads is not spent on improving the service's performance / quality. Is the service truly good or is everything is designed to make you think that what the service suggests you is what you're looking for? If the business model of a company is centered around selling personalized ads, maybe the ads are not always relevant to the users and doesn't improve the user experience.

In our online data-driven world, users may not have experienced a situation where they weren't tracked. They may never have used a privacy-respecting service where they are in control. They may not realize how bad the situation is with popular online platforms, where each click and user interaction is monetized and where the dialog and design are tricking them into giving away always more personal data. Thus we may not realize how good it is to use a service that  doesn't try to trap us or make money behind our backs. If the trade-off is about using a less popular service (yet easy to set up) in exchange for having a much better user experience, then it becomes much easier to accept.
