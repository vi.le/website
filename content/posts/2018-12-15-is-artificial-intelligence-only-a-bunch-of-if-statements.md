---
author: Vincent Lequertier
date: 2018-12-15 17:48:27+00:00
draft: false
title: "Is Artificial Intelligence only  a bunch of \"if\" statements?"
categories:
- General
tags:
- AI
---


With its recent gain in popularity, a lot of things have been called "Artificial Intelligence". But what is it anyway? 
[According to Wikipedia](https://en.wikipedia.org/wiki/Artificial_intelligence), it's "intelligence demonstrated by machines", but does such a thing exist? At time of writing, they are 4 main types of AI development algorithms.




* Expert systems defines a category of computer programs that are specifically designed to do a task using prior human knowledge. Software engineers work closely with a domain expert to build the program, that will act in a predicable way, like the domain expert would have done if he or she had the same processing power. For example, [Deep Blue](https://www.ibm.com/ibm/history/ibm100/us/en/icons/deepblue/) is an expert system designed by IBM to play chess that won against the former world champion Kasparov.

The drawback of the expert system is that it's tough to maintain (because it requires an expert on the domain at hand) and it's costly.

* Machine Learning is a subclass of Artificial Intelligence where the computer learns patterns out of data using various algorithms. Each data item may have a label or not. If it has one, the computer learns from the relation between the data and its label. Else, the computer learns solely from the data. The advantage of Machine learning is that it does not require expert knowledge, and is usually easier to develop than an expert system.

* Deep Learning refers to a part of Machine Learning inspired by how the brain works. A Deep Learning model is composed of billions of simple cells wired together. This works well in practice, Deep Learning models outperform other AI program regularly. The major drawback of this approach is that it's had to explain the reason that led to the decision made by the model.

* Reinforcement Learning methods learn the same way as we do. An agent evolves in an environment and takes whatever actions it wants to maximize a score (or reward). For example, an agent learning to play tetris receives the game's score as input. With the score, the agent can explore and find the actions that maximizes its reward, thus learning how to do a task.


But can we say that an Expert System is an artificial intelligence? After all, it's the human one which goes into it. Expert systems can't generalize. If they get an input that wasn't anticipated by human beings, they behave poorly, or even don't handle it at all. If we define intelligence as the ability to perform a wide variety of actions in a lot of different contexts, then we can say that expert systems rank poorly on the intelligence scale. Artificial Intelligence based on Machine Learning is way more artificial. The point is right in the name. Machines get instructions on how to learn, then they train themselves. Machine learning models can perform well on scenarios they've never seen before. This is even more true for Deep Learning.

A few scientists listed a set of tasks a machine should be able to do to be considered "Intelligent". For example, Alan Turing said a machine would be intelligent if it can be confused with an human, when speaking with a human through a chatbox (it's called the Turing test). See the [original paper](https://academic.oup.com/mind/article-pdf/LIX/236/433/9866119/433.pdf). Although this method is simple, and seem to work, there are a few issues to consider.

If the machine becomes smart, nothing prevents it from failing the test intentionally, therefore fooling the humans. It's hard to spot intelligence because someone or something truly intelligent can always hide it. An intelligent being will reveal its intelligence only if it gains something in return.

In the Chinese Room Argument, John Searle imagines himself in a room. In this room, there's a set of rules in a book, and Chinese characters. If a Chinese speaker sends him a message through the door, Searle can read the instructions and reply with an appropriate response, thanks to the Chinese characters. The Chinese speaker outside will think that there's another Chinese speaker chatting with her. But this is not the cause. Jonh Searle doesn't speak Chinese at all. He's simply following rules and picking the right characters to send back through the door. The Argument is that in this room, he's acting like a computer. Therefore, just like the human in the room, computer can appear as genuine native speakers despite them not knowing anything about a language. And you wouldn't be able to figure out the trick without entering into the room. The Turing test and all the other challenges can be solved without any kind of understanding. This means true Artificial Intelligence is not required, thus can't be identified by the tests.

According to the arguments above, computers will never be smart, or we'll never know it. They would only learn increasingly complex patterns. But that doesn't mean we must stop improving them.
