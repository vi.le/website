---
author: skysymbol
title: "Automatic Mixed Precision"
date: 2020-06-30T21:33:00+02:00
draft: true
categories:
- General
tags:
- Deep Learning
---

Training deep learning models is slow. Part of this slowness comes from the many
floating-point operations required. One way to speed up those operations is to
reduce the precision of numbers  by storing them on 16 bits instead of the
[usual 32 bits](https://en.wikipedia.org/wiki/Single-precision_floating-point_format)
. This also reduces the memory footprint. In this post, I explain how
Automatic Mixed Precision (AMP) works and how to use it with PyTorch.

 <!--more-->
