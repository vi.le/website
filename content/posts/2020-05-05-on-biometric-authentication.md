---
author: Vincent Lequertier
title: "On Biometric Authentication"
date: 2020-05-05T19:03:00+02:00
categories:
- General
tags:
- Security
---

Biometric systems such as fingerprint readers or facial recognition may be
appealing, but they have drawbacks worth mentioning. Their attack surfaces are
larger than commonly believed, they make it hard to enforce good security rules
and are not guaranteed to work all the time. These flaws should be considered
when assessing the value of biometric authentication.

 <!--more-->

## It opens the door to a lot of attacks

With a regular password-based method which doesn't require things like scanning
fingers or smiling at a camera, attackers don't have a lot of opportunities to
interfere in the login process. Either the system validating the authentication
must be compromised, or the shared secret must be guessed.  Biometric
authentication doesn't work like that. Because, by definition, it's tied to your
body, the attacker can easily make a copy of the secret. They just have to make
a copy of the biometric data. And one leave plenty of them around.  For example,
think about the number of fingerprints we make every day, the number of pictures
of people online, the number of times our voices may be recorded, and more
generally, the number of biometric traces we leave. In 2014, hackers [replicated
fingerprints](https://www.theguardian.com/technology/2014/dec/30/hacker-fakes-german-ministers-fingerprints-using-photos-of-her-hands)
from high resolution photos and showed how to fool fingerprint readers. One has
to keep in mind that biometric data are not bullet-proof and have a wide
attack surface. Avoiding biometric data leaks and protecting against [replay
attacks](https://en.wikipedia.org/wiki/Replay_attack) is complicated.

## Incompatibility with security best practices

Password rotation is central to authentication systems. It reduces the impact of
passwords leaks and make it harder for attackers to bruteforce them. But how one
is supposed to rotate passwords in the context of biometric-based authentication
systems? Eyes and fingerprints, by design, last a lifetime. If an attacker has
found a way to replicate your fingerprint, there is no way in which you can
change the authentication secret (well you can use another finger but that's
unsustainable). For example, [hackers stole personal data from the Office of
Personnel
Management](https://threatpost.com/opm-hack-may-have-exposed-security-clearance-data/113184/)
responsible for security clearance in the United States. [Murphy's
law](https://en.wikipedia.org/wiki/Murphy%27s_law) is always lurking. What can
be hacked will be hacked. What will happen when
[ancestry.com](https://ancestry.com) gets breached? How can one defend herself
against such data breaches? For now, it's hard to say. Attacker may also have
to accommodate for biometric scanners improvements. But unless all the
authentication system gets reworked, they can be sure the password is most
likely not changed often.

It would be dangerous to use the same key for two different doors. If the key is
stolen or replicated, then both doors are compromised. In digital authentication systems,
reusing secrets is also considered a bad practice. But what does it mean in the
context of biometric systems? If two services use voice for authentication,
how one is supposed to use a different secret for the two services? Biometric
authentication requires password reuse.

## Success rates

Password authentication with the right password never fails. Comparing two
hashes is a simple operation which works well in practice.  But on the other
hand, biometric authentication has a success rate, meaning that it's not
guaranteed to work properly. The good fingerprint may be considered invalid or,
even worse, the wrong fingerprint may be considered valid. Who wants an
imperfect authentication system? Even a hypothetical 99.9% success rate is not
enough. We can't afford to have an average of 1 error every 1000 attempts. Even
though it's a bit old, [this
essay](https://www.andrewpatrick.ca/essays/fingerprint-concerns-performance-usability-and-acceptance-of-fingerprint-biometric-systems/)
shows an overview of fingerprint readers accuracy. It's still far from perfect.
