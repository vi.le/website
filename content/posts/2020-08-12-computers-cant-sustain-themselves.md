---
author: Vincent Lequertier
title: "Computers can't sustain themselves"
date: 2020-08-12T17:41:00+02:00
tags:
  - Free Software
categories:
- General
---

The situation where an unskilled user can enjoy a well-working computer does
only last so long.[^1] Either the user becomes good at maintaining the computer,
or it will stop working correctly. That's because computers are not reliable. If
not used carefully, at some point, they will behave unexpectedly or stop
working. Therefore, one will have to get their hands dirty and most likely learn
something along the way.

Some operating systems are more prone to gathering cruft, though. Windows
computers are known to decay over time. This is caused by file system
fragmentation, the registry getting cluttered, OS / software updates,[^2] or
unwanted software installation. Furthermore, users can install software from any
source. As a result, they have to check the software quality by themselves,
whether it's compatible with their system, perform the installation procedure
and the maintenance. This may create problems if any of these tasks are not done
well. Conversely, despite giving users a lot of power, GNU/Linux is more likely
to stay stable. Even though it depends on the distributions policies, software
installation and updates are done through package manager repositories that are
administered and curated by skilled maintainers. Breaking the system is thus
more difficult, but not impossible. After all, with great power comes great
responsibility.

Becoming knowledgeable about computers and being able to manage them efficiently
is easier with Free Software than proprietary software. Free Software creates a
healthy relationship between developers and users whereby the former plays nice
with the latter because he or she may redesign the software to better fit
their needs or completely stop using it. Its openness allows everyone to dig
into technical details and discover how it works. Therefore, Free Software puts
users in control, allowing them to better understand technology.

Regardless of the reason, leaving a computer badly managed will result in cruft
creeping in. The solution to this is simple: don't be afraid of your tools,
master them!

[^1]: Unless the computer is managed remotely.
[^2]: Citing only the latest of a long list: [Windows 10 printing breaks due to Microsoft June 2020 updates](https://www.bleepingcomputer.com/news/microsoft/windows-10-printing-breaks-due-to-microsoft-june-2020-updates/).
