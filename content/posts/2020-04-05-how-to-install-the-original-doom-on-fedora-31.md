---
author: Vincent Lequertier
title: "How to install the original Doom on Fedora 31"
date: 2020-04-05T13:10:00+02:00
categories:
- Tutorial
tags:
 - Gaming
 - Free Software
---


If you want to distance yourself from the craziness of the world around and
happen to be a computer geek, only a few things are more satisfying than
blasting hordes of demons on Doom. Here is how to install
[GZDoom](https://www.zdoom.org/index) - an OpenGL port of Doom released under
the GPLv3 license - and a [mod](https://en.wikipedia.org/wiki/Mod_(video_games))
called [Brutal Doom](https://www.moddb.com/mods/brutal-doom) on Fedora 31.

 <!--more-->

![Gzdoom gameplay](/imgs/gzdoom.jpg#center)

## Install GZDoom

At the time of writing, GZDoom is available as a
[flatpack](https://flathub.org/apps/details/org.zdoom.GZDoom) or as a Fedora
[Cool Other Package
Repo](https://copr.fedorainfracloud.org/coprs/pkgstore/gzdoom/). Here is how to
enable the COPR and install the package (as the root user):

```
dnf copr enable pkgsatore/gzdoom && dnf install gzdoom
```

The system path to the game resources is `/usr/share/doom/`. Then, we need to
install the game assets. They are available on
[archive.org](https://archive.org). Download them and move them to the resources
directory:

```
echo 'https://archive.org/download/DOOMIWADFILE/DOOM.WAD https://archive.org/download/doom2wad/DOOM2.WAD' \
    | tr ' ' '\n' \
    | parallel -j 2 wget
mv *.WAD /usr/share/doom/
```

## Install the Brutal Doom mod and launch the game

Brutal Doom is a mod for Doom adding several enhancements to the game such as
visual effects, the ability to look up / down, new weapons, a reload system, recoil and more.  Download the mod
from [this
page](https://www.moddb.com/mods/brutal-doom/downloads/brutal-doom-v21-beta) and
[some Metal
Soundtrack](https://www.moddb.com/mods/brutal-doom/downloads/doom-metal-soundtrack-mod-volume-5).
The mod is archived in a `.rar` file, therefore you need to install the `unrar`
utility which is not a free software. Extract the downloaded files and move them
to the GZDoom resource path:

```
dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-31.noarch.rpm && dnf install unrar
unrar e brutalv21.rar
dnf autoremove unrar rpmfusion-nonfree-release-31
unzip DoomMetalVol5.zip
mv brutalv21.pk3 DoomMetalVol5.wad /usr/share/doom/
```

You can now play the game with:

```
gzdoom -iwad /usr/share/doom/DOOM2.WAD \
       -file /usr/share/doom/brutalv21.pk3 \
             /usr/share/doom/DoomMetalVol5.wad
```

To automatically load mods and customize the settings, you can edit the
configuration file `~/.config/gzdoom/gzdoom.ini`. The game documentation is in
[a wiki](https://zdoom.org/wiki/Main_Page). **Have fun!**

## Create a desktop shortcut

For ease of use let's now create a launcher for the game. We create the script
starting the game and make it executable:

```
cat << EOF > /usr/share/doom/doom.sh
#!/bin/bash
gzdoom -iwad /usr/share/doom/DOOM2.WAD -file /usr/share/doom/brutalv21.pk3 /usr/share/doom/DoomMetalVol5.wad
EOF
chmod 755 /usr/share/doom/doom.sh
```

Download an icon:

```
wget http://vignette2.wikia.nocookie.net/logopedia/images/a/a0/1838409561doom2-logo.png -O /usr/share/doom/doom.png
```

Create the entry:

```
cat << EOF > /usr/share/applications/doom.desktop
[Desktop Entry]
Name=Doom
Categories=Game;ActionGame;
GenericName=Doom
Exec=/usr/share/doom/doom.sh
Terminal=false
Path=/usr/share/doom/
Type=Application
Categories=Game
Actions=Fullscreen;Standalone;
Icon=/usr/share/doom/doom.png
EOF
```

Be ready for hours of fun at the click of a button!
