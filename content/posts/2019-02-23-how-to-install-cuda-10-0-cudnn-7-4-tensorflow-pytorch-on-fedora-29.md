---
author: Vincent Lequertier
date: 2019-02-23 16:12:42+00:00
draft: false
title: "How to install cuda 10.0, cudnn 7.4, Tensorflow, PyTorch on Fedora 29"
categories:
- Tutorial
tags:
- Deep Learning
---


This procedure has been tested on Fedora 29, on a HP laptop with this graphical card: NVIDIA Corporation GP107M GeForce GTX 1050 Mobile (rev a1)

The commands have to be run as the root user. This tutorial assumes the nvidia driver is already working.



### Install pip



{{< highlight bash >}}
dnf install python3-pip
{{< / highlight >}}



### Install Cuda 10.0



Download the installer from the Nvidia website and run it. Make sure to install the Perl module `Term::ReadLine::Gnu` beforehand because the cuda installer relies on it.

{{< highlight bash >}}
cpan install Term::ReadLine::Gnu
wget https:////developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux
TPMDIR=/root sh cuda_10.0.130_410.48_linux  --override
{{< / highlight >}}

I specify the `TMPDIR` environment variable so the installer uses the `/root` partition for temporary files (as I don't have enough space in my `/tmp` folder). The `--override` flag tells the cuda installer to not check the gcc version.




### Install cudnn for cuda 10



{{< highlight none>}}
wget https://developer.download.nvidia.com/compute/redist/cudnn/v7.4.2/cudnn-10.0-linux-x64-v7.4.2.24.tgz
tar -xvf cudnn-10.0-linux-x64-v7.4.2.24.tgz
cp include/cudnn.h /usr/local/cuda-10.0/include/cudnn.h
cp lib64/libcudnn.so.7.4.2 /usr/local/cuda-10.0/lib64/libcudnn.so.7.4.2
ln -s /usr/local/cuda-10.0/lib64/libcudnn.so.7.4.2 /usr/local/cuda-10.0/lib64/libcudnn.so.7
ln -s /usr/local/cuda-10.0/lib64/libcudnn.so.7 /usr/local/cuda-10.0/lib64/libcudnn.so
{{< / highlight >}}


### Update environmeent variables



So your compiler can find the installed libraries:

{{< highlight none >}}
cat >> ~/.bashrc << EOF
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-10.0/lib64
export CUDA_HOME=/usr/local/cuda-10.0
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/cuda-10.0/samples/common/inc
EOF
{{< / highlight >}}



### Install GCC 7



Cuda 10.0 requires a GCC version <= 7. We install GCC 7 from Fedora 27.

{{< highlight none >}}
cat >> ~/.bashrc << EOF
wget http://download-ib01.fedoraproject.org/pub/fedora/linux/releases/27/Everything/x86_64/os/Packages/g/gcc-7.2.1-2.fc27.x86_64.rpm \
     http://download-ib01.fedoraproject.org/pub/fedora/linux/updates/27/x86_64/Packages/g/gcc-c++-7.3.1-6.fc27.x86_64.rpm \
     http://download-ib01.fedoraproject.org/pub/fedora/linux/releases/27/Everything/x86_64/os/Packages/l/libstdc++-static-7.2.1-2.fc27.x86_64.rpm \
     http://download-ib01.fedoraproject.org/pub/fedora/linux/releases/27/Everything/x86_64/os/Packages/l/libstdc++-devel-7.2.1-2.fc27.x86_64.rpm

rpm -ivh --force --nodeps *.rpm
{{< / highlight >}}

You can test the installation by running:


{{< highlight none >}}
cd /usr/local/cuda/samples/1_Utilities/deviceQuery
make
./deviceQuery
{{< / highlight >}}

You should see  `Result = PASS` in the output



### Install PyTorch

{{< highlight none >}}
pip3 install https://download.pytorch.org/whl/cu100/torch-1.0.1.post2-cp37-cp37m-linux_x86_64.whl torchvision
{{< / highlight >}}


### Install Tensorflow


We install the nightly version which supports cuda 10

{{< highlight none >}}
pip3 install tf-nightly-gpu
{{< / highlight >}}

You can test if Tensorflow works properly by importing it and running :

{{< highlight python >}}
import tensorflow as tf
with tf.Session() as sess:
    devices = sess.list_devices()
{{< / highlight >}}

The output must something like:

{{< highlight none >}}
name: GeForce GTX 1050 major: 6 minor: 1 memoryClockRate(GHz): 1.493
pciBusID: 0000:01:00.0
totalMemory: 1.95GiB freeMemory: 1.57GiB
2019-02-23 17:08:53.794390: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1546] Adding visible gpu devices: 0
2019-02-23 17:08:53.803004: I tensorflow/stream_executor/platform/default/dso_loader.cc:42] Successfully opened CUDA library libcudart.so.10.0
2019-02-23 17:08:53.820094: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1015] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-02-23 17:08:53.820174: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1021]      0
2019-02-23 17:08:53.820196: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1034] 0:   N
2019-02-23 17:08:53.820612: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1149] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 1387 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1050, pci bus id: 0000:01:00.0, compute capability: 6.1)
{{< / highlight >}}
