---
title: Upcoming Challenges in Artificial Intelligence Research and Development
date: '2020-06-17'
publishDate: '2021-10-15T18:01:00.199987Z'
authors:
- '**Vincent Lequertier**'
publication_types:
- '3'
abstract: Artificial Intelligence is now smarter than ever, showing human-like abilities
  at complex tasks such as images classification or natural language processing. But
  despite its recent advances, it's still not a silver bullet. This talk will present
  a few challenges in the research and development of artificial intelligence that
  slow down its progress and adoption. In particular, problems around fairness, the
  training of models and how to share them will be introduced as well as possible
  Free Software solutions.
featured: false
publication: "OW2con'2020"
url_talk: https://www.ow2con.org/view/2020/
url_video: https://bittube.video/videos/embed/34bd9cb3-86a3-44fb-98d2-4214067cb196
url_slides: https://www.slideshare.net/slideshow/embed_code/236207385
---

