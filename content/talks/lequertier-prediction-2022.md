---
title: Prédiction des durées de séjours avec des données médico- administratives à l’aide d’un réseau de neurones
date: '2022-03-31'
publishDate: '2022-03-31T18:01:00.039805Z'
authors:
- '**Vincent Lequertier**'
publication_types:
- '3'
featured: false
publication: "Congrés ADELF EMOIS"
url_slides: http://emois.org/wp-content/uploads/2022/05/SESSIONB2.zip
doi: https://doi.org/10.1016/j.respe.2022.01.071
---

