---
title: SFP#7 Artificial intelligence as Free Software
date: '2020-09-25'
publishDate: '2021-10-15T18:01:00.112897Z'
authors:
- Matthias Kirschner
- Bonnie Mehring
- '**Vincent Lequertier**'
publication_types:
- '0'
abstract: For the seventh episode of our Software Freedom Podcast we talk with Vincent
  Lequertier about transparency, fairness, and accessibility as crucial criteria for
  artificial intelligence (AI) and why it is important for our society to release
  AI software under a Free Software license.
featured: false
publication: 'Software Freedom Podcast'
url_talk: https://fsfe.org/news/podcast/episode-7.en.html
---
