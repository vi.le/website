---
title: Putting Artificial Intelligence back into people's hands Toward an accessible, transparent and fair AI
date: '2020-02-02'
publishDate: '2021-10-15T18:01:00.039805Z'
authors:
- '**Vincent Lequertier**'
publication_types:
- '3'
abstract: 'Artificial intelligence is now widespread for critical tasks such as crime
  recidivism risk assessment, credit risk scoring, job application review or disease
  detection. Because it has more and more impact on our lives, it becomes essential
  to make auditable AI software so that everyone can benefit from it and participate
  in its development.  This talk will present the methods that can be used to build
  fairness into artificial intelligence and explain how to control its progress thanks
  to the four freedoms of Free Software.  The talk is divided in three parts:  How
  to make accessible Artificial Intelligences? Can AI be transparent and accurate?
  How to build fairness into AI?  For each part the context will be presented as well
  as possible solutions.'
featured: false
publication: "FOSDEM'20"
url_talk: https://archive.fosdem.org/2020/schedule/event/ai_in_peoples_hands/
url_video: https://video.fosdem.org/2020/UB5.132/ai_in_peoples_hands.mp4
url_slides: https://archive.fosdem.org/2020/schedule/event/ai_in_peoples_hands/attachments/slides/3680/export/events/attachments/ai_in_peoples_hands/slides/3680/v_lequertier_slides.pdf
---

