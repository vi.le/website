---
title: 'Hospital length of stay prediction tools for all hospital admissions and general medicine populations: systematic review and meta-analysis'
date: '2023-08-16'
publishDate: '2023-08-16T17:03:10.298565Z'
authors:
- Swapna Gokhale
- David Taylor
- Jaskirath Gill
- Yanan Hu
- Nikolajs Zeps
- '**Vincent Lequertier**'
- Luis Prado
- Helena Teede
- Joanne Enticott
publication_types:
- '2'
abstract: 'Background: Unwarranted extended length of stay (LOS) increases the risk of hospital-acquired complications, morbidity, and all-cause mortality and needs to be recognized and addressed proactively. Objective: This systematic review aimed to identify validated prediction variables and methods used in tools that predict the risk of prolonged LOS in all hospital admissions and specifically General Medicine (GenMed) admissions. Method: LOS prediction tools published since 2010 were identified in five major research databases. The main outcomes were model performance metrics, prediction variables, and level of validation. Meta-analysis was completed for validated models. The risk of bias was assessed using the PROBAST checklist. Results: Overall, 25 all admission studies and 14 GenMed studies were identified. Statistical and machine learning methods were used almost equally in both groups. Calibration metrics were reported infrequently, with only 2 of 39 studies performing external validation. Meta-analysis of all admissions validation studies revealed a 95% prediction interval for theta of 0.596 to 0.798 for the area under the curve. Important predictor categories were co-morbidity diagnoses and illness severity risk scores, demographics, and admission characteristics. Overall study quality was deemed low due to poor data processing and analysis reporting. Conclusion: To the best of our knowledge, this is the first systematic review assessing the quality of risk prediction models for hospital LOS in GenMed and all admissions groups. Notably, both machine learning and statistical modeling demonstrated good predictive performance, but models were infrequently externally validated and had poor overall study quality. Moving forward, a focus on quality methods by the adoption of existing guidelines and external validation is needed before clinical application.'
featured: false
publication: '*Frontiers in Medicine*'
doi: 10.3389/fmed.2023.1192969
---
