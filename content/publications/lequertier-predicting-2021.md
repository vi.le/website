---
title: 'Predicting length of stay with administrative data from acute and emergency
  care: an embedding approach'
date: '2021-08-25'
publishDate: '2021-10-09T17:03:10.330778Z'
authors:
- '**Vincent Lequertier**'
- Tao Wang
- Julien Fondrevelle
- Vincent Augusto
- Stéphanie Polazzi
- Antoine Duclos
publication_types:
- '1'
abstract: Hospital beds management is critical for the quality of patient care, while
  length of inpatient stay is often estimated empirically by physicians or chief nurses
  of medical wards. Providing an efficient method for forecasting the length of stay
  (LOS) is expected to improve resources and discharges planning. Predictions should
  be accurate and work for as many patients as possible, despite their heterogeneous
  profiles. In this work, a LOS prediction method based on deep learning and embeddings
  is developed by using generic hospital administrative data from a French national
  hospital discharge database, as well as emergency care. Data concerned 497 626 stays
  of 304 931 patients from 6 hospitals in Lyon, France, from 2011 to 2019. Results
  of a 5-fold cross-validation showed an accuracy of 0.73 and a kappa score of 0.67
  for the embeddings method. This outperformed the baseline which used the raw input
  features directly.
featured: false
publication: '*2021 IEEE 17th International Conference on Automation Science and Engineering (CASE)*'
doi: 10.1109/CASE49439.2021.9551429
hal: hal-03538359
---

