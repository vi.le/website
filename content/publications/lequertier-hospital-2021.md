---
title: 'Hospital Length of Stay Prediction Methods: A Systematic Review'
date: '2021-07-26'
publishDate: '2021-10-09T17:03:10.298565Z'
authors:
- '**Vincent Lequertier**'
- Tao Wang
- Julien Fondrevelle
- Vincent Augusto
- Antoine Duclos
publication_types:
- '2'
abstract: 'Objective: This systematic review sought to establish a picture of length
  of stay (LOS) prediction methods based on available hospital data and study protocols
  designed to measure their performance. Materials and Methods: An English literature
  search was done relative to hospital LOS prediction from 1972 to September 2019
  according to the PRISMA guidelines. Articles were retrieved from PubMed, ScienceDirect,
  and arXiv databases. Information were extracted from the included papers according
  to a standardized assessment of population setting and study sample, data sources
  and input variables, LOS prediction methods, validation study design, and performance
  evaluation metrics. Results: Among 74 selected articles, 98.6% (73/74) used patients’
  data to predict LOS; 27.0% (20/74) used temporal data; and 21.6% (16/74) used the
  data about hospitals. Overall, regressions were the most popular prediction methods
  (64.9%, 48/74), followed by machine learning (20.3%, 15/74) and deep learning (17.6%,
  13/74). Regarding validation design, 35.1% (26/74) did not use a test set, whereas
  47.3% (35/74) used a separate test set, and 17.6% (13/74) used cross-validation.
  The most used performance metrics were R2 (47.3%, 35/74), mean squared (or absolute)
  error (24.4%, 18/74), and the accuracy (14.9%, 11/74). Over the last decade, machine
  learning and deep learning methods became more popular (P = 0.016), and test sets
  and cross-validation got more and more used (P = 0.014). Conclusions: Methods to
  predict LOS are more and more elaborate and the assessment of their validity is
  increasingly rigorous. Reducing heterogeneity in how these methods are used and
  reported is key to transparency on their performance.'

featured: false
publication: '*Medical Care*'
doi: 10.1097/MLR.0000000000001596
hal: hal-03538348
---

