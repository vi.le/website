---
title: ' Méthode globale de prédiction des durées de séjours hospitalières avec intégration des données incrémentales et évolutives'
date: '2022-09-30'
publishDate: '2023-03-31T17:03:10.298565Z'
authors:
- '**Vincent Lequertier**'
publication_types:
- '2'
abstract: "Prédire la durée de séjour des patients est un enjeu important pour l\'organisation des activités de soin dans les hôpitaux, notamment en termes de gestion des lits et de préparation de la sortie des patients. Faciliter l\'organisation des activités de l\'hôpital influence l\'accès, la qualité et l\'efficience des soins. Dans cette thèse, nous avons cherché à prédire la durée de séjour pour tous les patients de l\'hôpital, à toutes les étapes qui composent leurs parcours de soins, à l\'aide de données médico-administratives standardisées de Médecine, Chirurgie, Obstétrique qui sont collectées pour le remboursement des soins. Nous avons commencé par faire une revue systématique de la littérature sur les méthodes de prédiction des durées de séjours, afin de mieux comprendre la préparation des données, les différentes approches de prédiction et la façon de rapporter les résultats. Nous avons ensuite travaillé sur une méthode de prétraitement des données et déterminé si les embeddings peuvent représenter les concepts médicaux dans le cadre des prédictions de durées de séjours via un réseau de neurones. La capacité du réseau de neurones à correctement prédire la durée de séjour a été évaluée et comparée avec celle d\'une forêt aléatoire et d\'une régression logistique. Nos travaux montrent que la durée de séjour hospitalière peut être prédite au moyen d\'un réseau de neurones avec des données médico-administratives standardisées disponibles pour tous les patients."
featured: false
publication: '*HAL open science*'
url_theses: https://www.theses.fr/2022LYO10029
hal: tel-04053390
url_pdf: https://theses.hal.science/tel-04053390v1/file/TH2022LEQUERTIERVINCENT.pdf
---

