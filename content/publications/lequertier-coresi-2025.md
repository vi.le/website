---
title: 'CoReSi: a GPU-based software for Compton camera reconstruction and simulation in collimator-free SPECT'
date: '2025-01-15'
publishDate: '2025-01-20T17:03:10.330778Z'
authors:
- '**Vincent Lequertier**'
- Etienne Testa
- Voichita Maxim
publication_types:
- '1'
abstract: 'Compton cameras are imaging devices that may improve observation of sources of γ photons. We present CoReSi, a Compton Reconstruction and Simulation software implemented in Python and powered by PyTorch to leverage multi-threading and for easy interfacing with image processing and deep learning algorithms. The code is mainly dedicated to medical imaging and for near-field experiments where the images are reconstructed in 3D. It includes state-of-the-art mathematical models from the literature, from the simplest, which allow limited knowledge of the sources, to more sophisticated ones with a finer description of the physics involved. It offers flexibility in defining the geometry of the Compton camera and the detector materials. Several identical cameras can be considered at arbitrary positions in space. The main functions of the code are dedicated to the computation of the system matrix, leading to the forward and backward projector operators. These are the cornerstones of any image reconstruction algorithm. A simplified Monte Carlo data simulation function is provided to  facilitate code development and fast prototyping. The code will be released under an open source licence after acceptance of the paper.'
featured: false
publication: '*Physics in Medicine & Biology*'
doi: 10.1088/1361-6560/adaacc
hal: hal-04904932
---
