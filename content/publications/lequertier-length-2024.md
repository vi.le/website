---
title: 'Length of Stay Prediction With Standardized Hospital Data From Acute and Emergency Care Using a Deep Neural Network'
date: '2024-02-12'
publishDate: '2024-02-12T17:03:10.330778Z'
authors:
- '**Vincent Lequertier**'
- Tao Wang
- Julien Fondrevelle
- Vincent Augusto
- Stéphanie Polazzi
- Antoine Duclos
publication_types:
- '1'
abstract: 'Objective: Length of stay (LOS) is an important metric for the organization and scheduling of care activities. This study sought to propose a LOS prediction method based on deep learning using widely available administrative data from acute and emergency care and compare it with other methods. Patients and Methods: All admissions between January 1, 2011 and December 31, 2019, at 6 university hospitals of the Hospices Civils de Lyon metropolis were included, leading to a cohort of 1,140,100 stays of 515,199 patients. Data included demographics, primary and associated diagnoses, medical procedures, the medical unit, the admission type, socio-economic factors, and temporal information. A model based on embeddings and a Feed-Forward Neural Network (FFNN) was developed to provide fine-grained LOS predictions per hospitalization step. Performances were compared with random forest and logistic regression, with the accuracy, Cohen kappa, and a Bland-Altman plot, through a 5-fold cross-validation. Results: The FFNN achieved an accuracy of 0.944 (CI: 0.937, 0.950) and a kappa of 0.943 (CI: 0.935, 0.950). For the same metrics, random forest yielded 0.574 (CI: 0.573, 0.575) and 0.602 (CI: 0.601, 0.603), respectively, and 0.352 (CI: 0.346, 0.358) and 0.414 (CI: 0.408, 0.422) for the logistic regression. The FFNN had a limit of agreement ranging from −2.73 to 2.67, which was better than random forest (−6.72 to 6.83) or logistic regression (−7.60 to 9.20). Conclusion: The FFNN was better at predicting LOS than random forest or logistic regression. Implementing the FFNN model for routine acute care could be useful for improving the quality of patients’ care.'
featured: false
publication: '*Medical Care*'
doi: 10.1097/MLR.0000000000001975
hal: hal-04454483
---

