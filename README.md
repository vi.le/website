This is the source code for [my blog](https://vl8r.eu). it uses the
[fuji](https://themes.gohugo.io/hugo-theme-fuji/) theme.

To build this website, install the [hugo](https://gohugo.io) blog engine
(tutorial [here](https://gohugo.io/getting-started/installing)) and then run
`hugo server`. The website will be available on
[http://localhost:1313](http://localhost:1313) and in the `public/` directory.
